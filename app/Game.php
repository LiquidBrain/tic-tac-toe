<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Game extends Model
{
    protected $fillable = ['x_size', 'y_size', 'human_mark', 'bot_mark', 'is_finished'];

    /**
     * One game has many rounds
     *
     * @return HasMany
     */
    public function rounds()
    {
        return $this->hasMany(Round::class);
    }

    /**
     * Get array of win options
     *
     * @return array
     */
    public function getWinOptions()
    {
        $winOptions = [];

        $xSize = $this->x_size;
        $ySize = $this->y_size;
        $cellsToWin = 3;

        for ($y = 1; $y <= $ySize; $y++) {  //horizontal
            for ($x = 1; $x <= $xSize; $x++) {

                $tempArr = [];
                for ($cellCount = 0; $cellCount < $cellsToWin; $cellCount++) {
                    if ($x + $cellCount <= $xSize) {
                        $tempArr[$cellCount + 1] = [$x + $cellCount, $y];
                    } else {
                        $tempArr = [];
                        break;
                    }
                }

                if (!empty($tempArr)) {
                    $winOptions[] = $tempArr;
                }
            }
        }

        for ($y = 1; $y <= $ySize; $y++) {  //vertical
            for ($x = 1; $x <= $xSize; $x++) {
                $tempArr = [];
                for ($cellCount = 0; $cellCount < $cellsToWin; $cellCount++) {
                    if ($y + $cellCount <= $ySize) {
                        $tempArr[$cellCount + 1] = [$x, $y + $cellCount];
                    } else {
                        $tempArr = [];
                        break;
                    }
                }

                if (!empty($tempArr)) {
                    $winOptions[] = $tempArr;
                }
            }
        }

        for ($y = 1; $y <= $ySize; $y++) {  //diaglonal
            for ($x = 1; $x <= $xSize; $x++) {
                $tempArr = [];

                for ($cellCount = 0; $cellCount < $cellsToWin; $cellCount++) {
                    if ($y + $cellCount <= $ySize && $x + $cellCount <= $xSize) {
                        $tempArr[$cellCount + 1] = [$x + $cellCount, $y + $cellCount];
                    } else {
                        $tempArr = [];
                        break;
                    }
                }

                if (!empty($tempArr)) {
                    $winOptions[] = $tempArr;
                }

                $tempArr = [];

                for ($cellCount = 0; $cellCount < $cellsToWin; $cellCount++) {
                    if ($y + $cellCount <= $ySize && $x + $cellCount <= $xSize) {
                        $tempArr[$cellCount + 1] = [$x + $cellCount, $ySize - $cellCount];
                    } else {
                        $tempArr = [];
                        break;
                    }
                }

                if (!empty($tempArr)) {
                    $winOptions[] = $tempArr;
                }
            }
        }

        return $winOptions;
    }

    /**
     * Get list of marks by human and bot
     *
     * @return array
     */
    public function getRoundsByUser()
    {
        $humanMark = $this->human_mark;
        $botMark = $this->bot_mark;

        $humanRounds = $this->rounds()->where('mark', $humanMark)->select(['pos_y', 'pos_x'])->get()->map(function ($item) {
            return array_values($item->toArray());
        })->toArray();

        $botRounds = $this->rounds()->where('mark', $botMark)->select(['pos_y', 'pos_x'])->get()->map(function ($item) {
            return array_values($item->toArray());
        })->toArray();

        return [
            'humanRounds' => $humanRounds,
            'botRounds' => $botRounds
        ];
    }

    /**
     * List of all available cell for game
     *
     * @return array
     */
    public function getAllCells()
    {
        $cells = [];

        for ($i = 1; $i <= $this->y_size; $i++) {
            for ($j = 1; $j <= $this->y_size; $j++) {
                $cells[$i][] = [$i, $j];
            }
        }

        return $cells;
    }

    /**
     * Get random available cell
     *
     * @return mixed
     */
    public function getBotMove()
    {
        $availableCells = [];
        $allCells = $this->getAllCells();
        $bookedCells = $this->rounds()->select(['pos_y', 'pos_x'])->get()->map(function ($item) {
            return array_values($item->toArray());
        })->toArray();

        foreach ($allCells as $row) {
            foreach ($row as $cell) {
                $isAvailable = true;

                foreach ($bookedCells as $booked) {
                    if ($cell == $booked) {
                        $isAvailable = false;
                        break;
                    }
                }

                if ($isAvailable) {
                    $availableCells[] = $cell;
                }
            }
        }

        $availableCount = sizeof($availableCells);

        return $availableCells[rand(0, $availableCount - 1)];
    }

    /**
     * Check if exists winner for game
     *
     * @return bool|string
     */
    public function getWinner()
    {
        $winOptions = $this->getWinOptions();
        $gameRounds = $this->getRoundsByUser();

        foreach ($winOptions as $option) {
            $botMatches = 0;
            $humanMatches = 0;

            foreach ($option as $rules) {
                //Check human rounds
                foreach ($gameRounds['humanRounds'] as $round) {
                    if ($rules == $round) {
                        $humanMatches++;
                    }
                }

                //Check bot rounds
                foreach ($gameRounds['botRounds'] as $round) {
                    if ($rules == $round) {
                        $botMatches++;
                    }
                }
            }

            if ($botMatches === sizeof($option)) {
                return 'Bot';
            } else if ($humanMatches === sizeof($option)) {
                return 'Human';
            }
        }

        return false;
    }
}
