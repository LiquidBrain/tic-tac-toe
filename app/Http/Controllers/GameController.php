<?php

namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class GameController extends Controller
{

    /**
     * Create new game
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function start(Request $request)
    {
        $data = $request->request->all();

        $humanMark = $data['human_mark'];
        $size = $data['size'];

        $game = new Game([
            'x_size' => $size,
            'y_size' => $size,
            'human_mark' => $humanMark,
            'bot_mark' => $humanMark === 'X' ? 'O' : 'X',
            'is_finished' => false
        ]);

        $game->save();

        return new JsonResponse($game);
    }

    /**
     * Make bot move
     *
     * @param Request $request
     * @param integer $gameId
     * @return JsonResponse
     */
    public function makeBotMove(Request $request, $gameId)
    {
        /**
         * @var Game $game
         */
        $game = Game::find($gameId);

        if ($game->is_finished) {
            return abort(400);
        }

        $move = $game->getBotMove();

        $posY = $move[0];
        $posX = $move[1];
        $mark = $game->bot_mark;

        $data = $this->move($posY, $posX, $mark, $game);

        return new JsonResponse($data);
    }

    /**
     * Make human move
     *
     * @param Request $request
     * @param integer $gameId
     * @return JsonResponse
     */
    public function makeMove(Request $request, $gameId)
    {
        /**
         * @var Game $game
         */
        $game = Game::find($gameId);

        if ($game->is_finished) {
            return abort(400);
        }

        $posY = $request->request->get('pos_y');
        $posX = $request->request->get('pos_x');
        $mark = $game->human_mark;

        $data = $this->move($posY, $posX, $mark, $game);

        return new JsonResponse($data);
    }

    /**
     * Make move
     *
     * @param integer $posY
     * @param integer $posX
     * @param string $mark
     * @param Game $game
     * @return array
     */
    private function move($posY, $posX, $mark, $game)
    {
        $round = $game->rounds()
            ->where('pos_x', $posX)
            ->where('pos_y', $posY)
            ->first();

        if ($round) {
            abort(400);
        }

        $game->rounds()->create(['pos_y' => $posY, 'pos_x' => $posX, 'mark' => $mark]);

        $data = [];
        $data['game'] = $game;
        $data['pos_y'] = $posY;
        $data['pos_x'] = $posX;
        $data['mark'] = $mark;
        $data['winner'] = $game->getWinner();

        if ($data['winner'] || $game->rounds->count() >= $game->x_size * $game->x_size) {
            $game->is_finished = true;
            $game->save();
        }

        return $data;
    }
}
