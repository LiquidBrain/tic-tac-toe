<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller
{
    /**
     * Login user by credentials
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->request->all();

        if (Auth::once($credentials)) {
            $user = Auth::getUser();
        } else {
            // Invalid user credentials handler
        }

        return new JsonResponse($user);
    }
}
