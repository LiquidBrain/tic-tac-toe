<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Round extends Model
{
    protected $fillable = ['id', 'game_id', 'mark', 'pos_x', 'pos_y'];

    public function game()
    {
        return $this->belongsTo(Game::class);
    }
}
