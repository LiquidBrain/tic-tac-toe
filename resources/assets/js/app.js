import Vue from 'vue'
import Game from './components/Game.vue'

window.Event = new Vue();

new Vue({
    el: '#app',
    render: h => h(Game)
});