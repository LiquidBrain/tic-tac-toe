<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::post('/login', 'LoginController@login');

Route::group(['middleware' => ['auth:api']], function () {

    Route::post('/game/start', 'GameController@start');
    Route::post('/game/{game}/move', 'GameController@makeMove');
    Route::post('/game/{game}/bot/move', 'GameController@makeBotMove');

});